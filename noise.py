import numpy as np
import matplotlib.pyplot as plot
from prettytable import PrettyTable


def function_graf(x):
    return np.sin(x) + 0.5


def function_noise(y, noise):
    return np.array([i + np.random.uniform(-noise, noise) for i in y])


def chebyshev_noise(filtered):
    return np.array([filtered[i] - filtered[i - 1] for i in range(1, len(filtered))]).max()


def chebyshev_diff(filtered, noised):
    assert len(filtered) == len(noised)
    return np.array(filtered - noised).max()


def chebyshev_distance(x, y):
    return np.array([x, y]).max()


def average_geom(values, weights):
    search_size = len(weights)
    result = values.copy()
    m = search_size // 2

    for i in range(m, len(values) - m):
        for j in range(i - m, i + m + 1):
            local_mul = np.array([values[j] ** weights[j - i + m]
                                  for j in range(i - m, i + m + 1)]).prod()
            result[i] = local_mul

    return result


def random_weights(size):
    weights = np.zeros(size)
    m = (size - 1) // 2
    weights[m] = "%.4f"%np.random.uniform(0, 1)

    for i in range(m):
        weights[i] = "%.4f"%(np.random.uniform(0, 1 - np.array([weights[j]
                                           for j in range(i + 1, size - 1 - i)]).sum())*0.5)
        weights[size - 1 - i] = "%.4f"%weights[i]
        weights[0] = "%.4f"%((1 - np.array([weights[j]
                                    for j in range(1, size - 1)]).sum()) * 0.5)
        weights[size - 1] = "%.4f"%weights[0]

    return weights


def function_filtering(N, r, y_noise):
    min = 999999
    p = PrettyTable(["h", "dis", "alpha", "w", "d"])
    w = []
    d = []

    for h in np.arange(0, 1.1, 0.1):
        result = None
        for i in range(0, N):
            weight_rnd = random_weights(r)
            y_filter = average_geom(y_noise, weight_rnd)
            noise = chebyshev_noise(y_filter)
            difference = chebyshev_diff(y_filter, y_noise)
            J = h * noise - (1 -h) * difference

            if result is None or result > J:
                weights = weight_rnd.copy()
                noise_min = noise
                difference_min = difference
                distance = chebyshev_distance(noise, difference)
                J_min = J
        if min > distance:
            min = distance
            h_m = h
            J_m = J_min
            w_m = noise_min
            d_m = difference_min

        w.append(noise_min)
        d.append(difference_min)

        p.add_row(["%.1f"%h, "%.4f"%distance, weights, "%.4f"%noise_min, "%.4f"%difference_min])
    print(p)
    m = PrettyTable(["h*", "J", "w", "d"])
    m.add_row(["%.1f"%h_m, "%.4f"%J_m, "%.4f"%w_m, "%.4f"%d_m])
    print(m)
    return y_filter, w, d


def draw_graf(y, y_noise, y_filter, w, d):
    plot.figure()
    plot.subplot(2, 1, 1)
    plot.plot(x, y, color='blue', label='Original')
    plot.plot(x, y_noise, color='#bfaaaa', label='Noised')
    plot.plot(x, y_filter, color='red', label='Filtered')
    plot.grid()
    plot.legend()

    plot.subplot(2, 1, 2)
    for i in range(10):
        color = f'#{np.random.randint(0, 0xff):02X}{np.random.randint(0, 0xff):02X}{np.random.randint(0, 0xff):02X}'
        plot.scatter(w[i], d[i], c=color)
    plot.grid()
    plot.legend()
    plot.show()

if __name__ == "__main__":
    xmin=0
    xmax=np.pi
    K_count=100
    amplitude_of_noise = 0.25
    P = 0.95
    eps = 0.01
    r3 = 3
    r5 = 5

    x = np.arange(xmin, xmax, (xmax-xmin)/(K_count+1))
    y = function_graf(x)
    y_noise = function_noise(y, amplitude_of_noise)
    N = int(np.log(1 - P) / np.log(1 - eps/(xmax - xmin)))
    y_filter, w, d = function_filtering(N, r3, y_noise)

    draw_graf(y, y_noise, y_filter, w, d)